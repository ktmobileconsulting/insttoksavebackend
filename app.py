from flask import Flask, request
import requests
import properties
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep

app = Flask(__name__)


def parseUrlLink(urlLink):
    return requests.get(
        properties.server_url + '/api/sendAns',
        params={
            "urlLink": urlLink,
        }
    )


@app.route('/')
def hello():
    print("hello")
    return {"hello": "hello"}


@app.route('/api/sendAns', methods=['GET'])
def parseUrlLink():
    link_from_app = request.args.get('urlLink')
    print(link_from_app)
    current_link = ""
    current_id = ""
    current_text = ""
    if "tiktok.com" in link_from_app:
        current_link = "https://snaptik.app/en"
        current_id = "url"
        current_text = "Download Server 01"
    elif "instagram" in link_from_app:
        current_link = "https://saveinsta.app/en1"
        current_id = "s_input"
        current_text = "Download Video"

    user_agent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2'
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f'user-agent={user_agent}')
    chrome_options.add_argument("--headless")

    webdriver1 = webdriver.Chrome(chrome_options=chrome_options, executable_path='/usr/bin/chromedriver')
    webdriver1.get(current_link)

    input = webdriver1.find_element("id", current_id)
    input.send_keys(link_from_app)
    input.send_keys(Keys.ENTER)
    sleep(5)

    links = webdriver1.find_elements(By.TAG_NAME, "a")
    result = ""
    for link in links:
        if link.text == current_text:
            print(link.get_attribute('href'))
            result = link.get_attribute('href')
    webdriver1.close()

    return {"linkDownload": result}


if __name__ == '__main__':
    app.run()
